/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltt.entity;

import java.util.Date;

/**
 *
 * @author Kira
 */
public class GD {

    private int ma;
    private int maLoaiGD;
    private Date ngay;
    private long soTien;
    private String noiDung;
    private String TKCo;
    private String TKNo;
    private int maKHNG;
    private int maKHNN;

    public GD() {
    }

    public GD(int ma, int maLoaiGD, Date ngay, long soTien, String noiDung, String TKCo, String TKNo, int maKHNG, int maKHNN) {
        this.ma = ma;
        this.maLoaiGD = maLoaiGD;
        this.ngay = ngay;
        this.soTien = soTien;
        this.noiDung = noiDung;
        this.TKCo = TKCo;
        this.TKNo = TKNo;
        this.maKHNG = maKHNG;
        this.maKHNN = maKHNN;
    }

    public int getMa() {
        return ma;
    }

    public void setMa(int ma) {
        this.ma = ma;
    }

    public int getMaLoaiGD() {
        return maLoaiGD;
    }

    public void setMaLoaiGD(int maLoaiGD) {
        this.maLoaiGD = maLoaiGD;
    }

    public Date getNgay() {
        return ngay;
    }

    public void setNgay(Date ngay) {
        this.ngay = ngay;
    }

    public long getSoTien() {
        return soTien;
    }

    public void setSoTien(long soTien) {
        this.soTien = soTien;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getTKCo() {
        return TKCo;
    }

    public void setTKCo(String TKCo) {
        this.TKCo = TKCo;
    }

    public String getTKNo() {
        return TKNo;
    }

    public void setTKNo(String TKNo) {
        this.TKNo = TKNo;
    }

    public int getMaKHNG() {
        return maKHNG;
    }

    public void setMaKHNG(int maKHNG) {
        this.maKHNG = maKHNG;
    }

    public int getMaKHNN() {
        return maKHNN;
    }

    public void setMaKHNN(int maKHNN) {
        this.maKHNN = maKHNN;
    }

}
