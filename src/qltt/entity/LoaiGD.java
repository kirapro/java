/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltt.entity;

/**
 *
 * @author Kira
 */
public class LoaiGD {
    private int maLoaiGD;
    private String tenLoaiGD;

    public LoaiGD() {
    }

    public LoaiGD(int maLoaiGD, String tenLoaiGD) {
        this.maLoaiGD = maLoaiGD;
        this.tenLoaiGD = tenLoaiGD;
    }

    public int getMaLoaiGD() {
        return maLoaiGD;
    }

    public void setMaLoaiGD(int maLoaiGD) {
        this.maLoaiGD = maLoaiGD;
    }

    public String getTenLoaiGD() {
        return tenLoaiGD;
    }

    public void setTenLoaiGD(String tenLoaiGD) {
        this.tenLoaiGD = tenLoaiGD;
    }
    
    
}
