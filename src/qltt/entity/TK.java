/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltt.entity;

/**
 *
 * @author Kira
 */
public class TK {
    private String soTK;
    private long soDu;
    private int maKH;
    private int maCN;

    public TK() {
    }

    public TK(String soTK, long soDu, int maKH, int maCN) {
        this.soTK = soTK;
        this.soDu = soDu;
        this.maKH = maKH;
        this.maCN = maCN;
    }

    public String getSoTK() {
        return soTK;
    }

    public void setSoTK(String soTK) {
        this.soTK = soTK;
    }

    public long getSoDu() {
        return soDu;
    }

    public void setSoDu(long soDu) {
        this.soDu = soDu;
    }

    public int getMaKH() {
        return maKH;
    }

    public void setMaKH(int maKH) {
        this.maKH = maKH;
    }

    public int getMaCN() {
        return maCN;
    }

    public void setMaCN(int maCN) {
        this.maCN = maCN;
    }
    
    
}
