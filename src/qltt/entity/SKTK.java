/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltt.entity;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Kira
 */
public class SKTK {

    private KH kh;
    private CN cn;
    private TK tk;
    private Date ngayThucHien;
    private Date tuNgay;
    private Date denNgay;
    private long soDuDauKy;
    private ArrayList<GD> gds;

    public SKTK(KH kh, CN cn, TK tk, Date ngayThucHien, Date tuNgay, Date denNgay, long soDuDauKy, ArrayList<GD> gds) {
        this.kh = kh;
        this.cn = cn;
        this.tk = tk;
        this.ngayThucHien = ngayThucHien;
        this.tuNgay = tuNgay;
        this.denNgay = denNgay;
        this.soDuDauKy = soDuDauKy;
        this.gds = gds;
    }

    public SKTK() {
    }

    public KH getKh() {
        return kh;
    }

    public void setKh(KH kh) {
        this.kh = kh;
    }

    public CN getCn() {
        return cn;
    }

    public void setCn(CN cn) {
        this.cn = cn;
    }

    public TK getTk() {
        return tk;
    }

    public void setTk(TK tk) {
        this.tk = tk;
    }

    public Date getNgayThucHien() {
        return ngayThucHien;
    }

    public void setNgayThucHien(Date ngayThucHien) {
        this.ngayThucHien = ngayThucHien;
    }

    public Date getTuNgay() {
        return tuNgay;
    }

    public void setTuNgay(Date tuNgay) {
        this.tuNgay = tuNgay;
    }

    public Date getDenNgay() {
        return denNgay;
    }

    public void setDenNgay(Date denNgay) {
        this.denNgay = denNgay;
    }

    public long getSoDuDauKy() {
        return soDuDauKy;
    }

    public void setSoDuDauKy(long soDuDauKy) {
        this.soDuDauKy = soDuDauKy;
    }

    public ArrayList<GD> getGds() {
        return gds;
    }

    public void setGds(ArrayList<GD> gds) {
        this.gds = gds;
    }

}
