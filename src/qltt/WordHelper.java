/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

/**
 *
 * @author Kira
 */
public class WordHelper {

    public static void writeSach(File file, ArrayList<Sach> list, String tittle) throws FileNotFoundException, IOException {
        XWPFDocument document = loadHeader(tittle);
        createTableSach(document, list);
        loadFooter(document);
        FileOutputStream out = new FileOutputStream(file);
        document.write(out);//ghi lại
        document.close();
        out.close();
    }

    /**
     * @param b : là thể hiện có in đậm hay không tham số s là nội dung
     * @param s : set nội dung
     */
    private static void format(XWPFTableCell cell, String s, boolean b) {
        cell.setVerticalAlignment(XWPFVertAlign.CENTER);// chính giữa theo chiều cao
        XWPFParagraph p = cell.getParagraphs().get(0);//lấy doạn văn bản
        p.setIndentationLeft(200);// tương đượng padding left
        p.setIndentationRight(200);// tương đượng padding right
        p.setAlignment(ParagraphAlignment.CENTER);// căn giữa văn bản
        XWPFRun r = p.createRun();// nội dung
        r.setBold(b);
        r.setFontFamily("Times New Roman");//set Kiểu chữ
        r.setFontSize(13);//set size text
        r.setColor("000000");// set color text
        r.setText(s);// set content text
    }

    private static XWPFDocument loadHeader(String tittle) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(new File("C:\\Users\\Kira\\Documents\\tittle.docx"));
        // load header
        XWPFDocument document = new XWPFDocument(fis);
        List<XWPFParagraph> paragraphList = document.getParagraphs();

        // tạo tiêu đề biểu mẫu
        XWPFParagraph paragraphTittle = document.createParagraph();
        paragraphTittle.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runTittle = paragraphTittle.createRun();
        runTittle.setBold(true);
        runTittle.setItalic(true);
        runTittle.setFontFamily("Times New Roman");//set Kiểu chữ
        runTittle.setFontSize(16);//set size text
        runTittle.setColor("000000");// set color text
        runTittle.setText(tittle);// set content text
        runTittle.addBreak();
        return document;
    }

    private static void createTableSach(XWPFDocument document, ArrayList<Sach> list) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(300)); // set chiều rộng

        createNewCell(tittleRow, "Mã", 300, 1);
        createNewCell(tittleRow, "Tên sách", 2500, 2);
        createNewCell(tittleRow, "Loại sách", 2500, 3);
        createNewCell(tittleRow, "Tác giả", 2500, 4);
        createNewCell(tittleRow, "Nhà xuất bản", 2500, 5);
        createNewCell(tittleRow, "Năm xuất bản", 2500, 6);
        createNewCell(tittleRow, "Vị trí", 2500, 7);
        createNewCell(tittleRow, "Số lượng", 300, 8);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            Sach o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getMa() + "", false);
            format(row.getCell(2), o.getTen() + "", false);
            format(row.getCell(3), o.getLoaiSach() + "", false);
            format(row.getCell(4), o.getTacGia() + "", false);
            format(row.getCell(5), o.getNhaXuatBan() + "", false);
            format(row.getCell(6), o.getNamXuatBan() + "", false);
            format(row.getCell(7), o.getViTri() + "", false);
            format(row.getCell(8), o.getSoLuong() + "", false);
        }
    }

    private static void loadFooter(XWPFDocument document) {
        // tạo footer
        XWPFParagraph paragraphFooter = document.createParagraph();
//        paragraphFooter.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runF1 = paragraphFooter.createRun();
        runF1.addBreak();
        runF1.setBold(true);
        runF1.setFontFamily("Times New Roman");//set Kiểu chữ
        runF1.setFontSize(12);//set size text
        runF1.setColor("000000");// set color text
        runF1.setText("                           Người lập                                            "
                + "                               Xác nhận của thủ thư");// set content text
        runF1.addBreak();

        XWPFRun runF2 = paragraphFooter.createRun();
        runF2.setItalic(true);
        runF2.setFontFamily("Times New Roman");//set Kiểu chữ
        runF2.setFontSize(12);//set size text
        runF2.setColor("000000");// set color text
        runF2.setText("                   (Ký và ghi rõ họ tên)                                            "
                + "                         (Ký và ghi rõ họ tên)");// set content text
    }

    private static void createNewCell(XWPFTableRow tittleRow, String s, int i, int stt) {
        tittleRow.addNewTableCell();// tạo cell mới
        format(tittleRow.getCell(stt), s, true);
        tittleRow.getCell(stt).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(i));
    }
}
